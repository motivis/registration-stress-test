require "thread"
require "watir"
require "yaml"
require "headless"
require_relative "watir-code/Register"

# Turn off Watir deprecations errors
Watir.logger.ignore :deprecations

# Create a stream to the resiles file
file = File.open("results.csv", "w")

# Load the users from the config file
users = YAML.load_file("template.yaml")

# Foreach user, tell them to register in a different thread
threads = []
users.each do |user|
  threads << Thread.new { register(user, file) }
end

threads.each { |x| x.join }

# Just so we know the code is finished
puts "done"
