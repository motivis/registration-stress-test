# Registration Stress Test

## Salesforce and User setup

1) Copy and paste the code from apex/resetpassword.apex into an execute anonymous window in salesforce.

2) set the motivis_lrm__Term_Academic_Calendar__c 

3) Set the offset if you are getting soql query errors (max is 150 DML statements)

4) Set the password that you want to have for your students

5) Run the code, and in the log, select file > Open Raw Log

6) Scroll down to the debug of the CSV string, and copy it

7) In the users directory, create a file called users.csv that has the content from step 6

8) Set the password that you used on step 4

9) Set the loginURL, termid, and regs variables

10) Fill in the array of courses you want on line 25

11) Run the code by navigating to the users directory

```bash
ruby script.rb
```

12) It will create a file called template.yaml that you will use to stress test

***

## Setup Ruby and Code Base
1) pull down code

2) Instal ruby by following guide here: https://gorails.com/setup

3) Install bundler gem: 

```bash 
gem install bundler
```
4) Install dependent gems (run from root directory):

```bash 
bundle install
```
5) create config file (if you ran "Salesforce and User setup", you can use the output from line 12 )

#### Template Config File 

___You can multiple users in a config.___

___For every user you have, that many users will be run___

```yaml
user1:
    :userName: <Users Login>
    :password: <Password>
    :loginURL: <Community Login URL>
    :termId: <The ID Of the Term wanting to register wth>
    :registration: <The Text of the drop down on the left hand nav to show terms>
    :courses: <Array of courses you want to register to>
user2:
    :userName: <Users Login>
    :password: <Password>
    :loginURL: <Community Login URL>
    :termId: <The ID Of the Term wanting to register wth>
    :registration: <The Text of the drop down on the left hand nav to show terms>
    :courses: <Array of courses you want to register to>
```
6) Save config in root folder.  Default name is template.yaml.

   * If you change the name, make sure to change line  14 in script.rb
   
7) Setup chromedriver: http://chromedriver.chromium.org/getting-started

8) To run the code, navigate to the root folder in a terminal and run

```bash
ruby script.rb
```
9) After you run, the results will be saved in the root folder with a file named "results.csv"
