def register(user_object, file)
  # Used to hide debug messages and to run as headless
  prod_run = true

  # Set the name of the student we are loggin in as
  name = user_object[0]
  # Set the user based on the entry in the config
  user = user_object[1]

  # Get Start time and log it and write it to results file
  start_time = Time.now
  p %Q(#{name},start time,#{start_time}) unless prod_run
  file.puts %Q(#{name},start time,#{start_time})

  #==================================
  # LOGIN
  #==================================
  browser = Watir::Browser.new :chrome, headless: prod_run
  browser.goto user[:loginURL]

  # username and pass
  browser.text_field(name: /username/).set user[:userName]
  browser.text_field(type: "password").set user[:password]

  # Click Login. Handles both Button and Link objects.
  if browser.button(name: /Login/i).present?
    browser.button(name: /Login/i).click!
  else
    browser.link(name: /login/).click!
  end

  #==================================
  # NAVIGATE TO REGISTER PAGE
  #==================================

  # Click the registration drop down on the left hand nav
  browser.link(title: /#{user[:registration]}/).wait_until_present.click
  # Click the term to register with
  browser.link(href: /#{user[:termId]}/).wait_until_present.click

  #==================================
  # TRY AND REGISTER
  #==================================

  # FOREACH COURSE IN THE CONFIG
  user[:courses].each do |course|
    course = course.to_s.strip

    # EXPAND THE COURSE OFFERINGS PANEL
    if browser.div(class: ["resize-column collapsed"], id: "coursesColumn").present?
      browser.div(class: ["resize-column collapsed"], id: "coursesColumn").link(class: ["resize-collapse-btn"]).click!
    else
      p "Courses Column was already expanded, so not expanding it." unless prod_run
    end

    # Search for course to ensure we are registering for the same course
    browser.div(class: ["table-filter-container"]).input.to_subtype.clear
    browser.div(class: ["table-filter-container"]).input.send_keys course

    # Grab the number of rows in the offerings table
    offerings_table = browser.div(id: "coursesColumn").wait_until_present.tbody.trs
    puts "Offering Table Row Count: " + offerings_table.count.to_s unless prod_run

    # A check for my courses to be empty.
    # Set the checked variable to 0
    # Then put the check for the table in a try catch.  If it can find it, set the var to the number of items
    # Else, set it to 0.
    original_course_count = 0
    puts "My Courses Table: " + browser.table(class: ["my-courses-table"]).present?.to_s unless prod_run
    course_table = browser.table(class: ["my-courses-table"]).present?
    if course_table
      begin
        my_courses_table = browser.table(class: ["my-courses-table"]).wait_until_present.tbody.trs
        puts "Courses Table Row Count: " + my_courses_table.count.to_s unless prod_run
        original_course_count = my_courses_table.count
      rescue
        original_course_count = 0
        puts "Courses Table Row Count: " + original_course_count.to_s unless prod_run
      end
    end

    # Without this sleep, we sometimes get stale element errors.
    sleep(3)

    row = offerings_table[0]
    # Setting timeout after searching for the course so we don't wait the full 30 seconds
    Watir.default_timeout = 10

    # Loop through all of the hidden records until we find the course that we want
    # If we don't find it, log it to the results, and move on to the next course
    x = 0
    begin
      while row.classes.include?("hidden") && x <= offerings_table.count
        puts "row class: " + row.classes.to_s unless prod_run
        row = offerings_table[x]
        puts x.to_s unless prod_run
        puts offerings_table.count.to_s unless prod_run
        x += 1
      end
    rescue Exception => e
      if x >= offerings_table.count
        file.puts %Q(#{name},#{course},#{Time.now},false,Course Was Not Found. Exception "#{e.to_s}")
        Watir.default_timeout = 30
        next
      end
    end
    Watir.default_timeout = 30
    # Course wasn't found

    # Grab and log the course title to ensure it's the one we are expecting
    course_title = row.tds[2].text.to_s
    p %Q(Course Title: #{course_title}) unless prod_run

    # Click add
    row.link(class: ["add-course-button"]).click!

    # Wait up to 10 seconds for AP creation (if it is going to be created)
    # Grab the error message from the toast message if there is one
    error_message = ""
    begin
      browser.div(class: ["snack-bar-message"]).wait_until(timeout: 10, &:present?) # Wait for up to 10 seconds for AP to be created
      puts "did it find the p tag: " + browser.div(class: ["snack-bar-message"]).p.to_s unless prod_run
      sleep(1.5) # Need to wait 1.5 seconds once the div is found for the text to be there to grab any error
      puts "P TEXT: " + browser.div(class: ["snack-bar-message"]).p.text unless prod_run
      error_message = browser.div(class: ["snack-bar-message"]).p.text
    rescue
      error_message = "none"
    end

    # Re-Init the offering table
    offerings_table = browser.div(id: "coursesColumn").wait_until_present.tbody.trs

    # Another check for empty state if the first course wasn't added
    begin
      my_courses_table = browser.table(class: ["my-courses-table"]).wait_until_present.tbody.trs
      new_course_count = my_courses_table.count
    rescue
      new_course_count = 0
    end

    # If the original course + 1 doesn't equal the new course count, then the course wasn't added successfully
    if (original_course_count + 1) == new_course_count
      p %Q(#{course_title} was added) unless prod_run
      file.puts %Q(#{name},#{course_title},#{Time.now},true,#{error_message})
    else
      p %Q(#{course_title} was not added) unless prod_run
      file.puts %Q(#{name},#{course_title},#{Time.now},false,#{error_message})
    end

    original_course_count = new_course_count
  end # End course loop

  #==================================
  # FINISH UP AND LOG
  #==================================

  # Log and write to results the end time and the total time that it took
  end_time = Time.now
  p %Q(#{name},end time,#{end_time}) unless prod_run
  file.puts %Q(#{name},end time,#{end_time})
  p %Q(#{name},total time,#{end_time - start_time}) unless prod_run
  file.puts %Q(#{name},total time,#{end_time - start_time})

  # Close the browser
  browser.close
end
