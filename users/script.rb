require "CSV"

# Load the users from the APEX export
users = CSV.read(%Q(#{File.dirname(__FILE__)}/users.csv>), { headers: false })

# Create a file stream to a yaml file
File.open("template.yaml", "w") { |file|
  # For every user, create a yaml entry
  users.each do |user|
    # Key based on the first name of the student
    key = user[0].gsub(" ", "").gsub(".", "").gsub("-", "")
    # Username is the username from the apex export
    username = user[1]
    password = "<What ever you set it to in the apex>"
    loginURL = "<Community Login URL>"
    termid = "<The ID Of the Term wanting to register wth>"
    regs = "<The Text of the drop down on the left hand nav to show terms>"

    file.write(%Q(#{key}:\r))
    file.write(%Q(     :userName: #{username}\r))
    file.write(%Q(     :password: #{password}\r))
    file.write(%Q(     :loginURL: #{loginURL}\r))
    file.write(%Q(     :termId: #{termid}\r))
    file.write(%Q(     :registration: #{regs}\r))
    file.write(%Q(     :courses: [<Array of courses you want to register to>]\r))
    file.write("\n")
  end
}
